# OpenML dataset: autoMpg

https://www.openml.org/d/42372

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Auto MPG (6 variables) dataset

The data concerns city-cycle fuel consumption in miles per gallon (Mpg), to be predicted in terms of 1 multivalued discrete and 5 continuous attributes (two multivalued discrete attributes (Cylinders and Origin) from the original dataset (autoMPG6) are removed).

This dataset is a slightly modified version of the dataset provided in the StatLib library. In line with the use by Ross Quinlan (1993) in predicting the attribute Mpg, 6 of the original instances were removed because they had unknown values for the Mpg attribute.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42372) of an [OpenML dataset](https://www.openml.org/d/42372). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42372/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42372/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42372/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

